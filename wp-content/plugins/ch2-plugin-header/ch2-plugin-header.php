<?php
/**
 * @package Chapter_2_-_Plugin_Header
 * @version 1.0
 */
/*
Plugin Name: Chapter 2 - Plugin Header
Plugin URI:
Description: Declares a plugin that will be visible in the WordPress admin interface
Author: Azri Adam
Version: 1.0
Author URI: http://azriadam.my/
*/