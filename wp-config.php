<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp-plugin-dev' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

if ( !defined('WP_CLI') ) {
    define( 'WP_SITEURL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
    define( 'WP_HOME',    $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
}



/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'TQdXNxIhtq0xzh2C9GuF3fh3jkS2xy4usfiMCqMMUYYJIr6kQeJdObJJj3UQf47e' );
define( 'SECURE_AUTH_KEY',  'u5WJwpMtHgDEke5FL7GjNKTNmZOCJ8dLbc5aFGj0p1mHe46uKBVYhkl0QP2NoSfa' );
define( 'LOGGED_IN_KEY',    'kosOHLABDkGtBWwIALA63ho1oo1j5PmEZuY0codBSsfXJj4M9AxkRLq4uffL5l83' );
define( 'NONCE_KEY',        '1yhxRScHi9Mi2zNF0oJ1yrWWB7i88x0TJmQyMq0sj9wmaKDtQkVjTo4bUwoNjc5K' );
define( 'AUTH_SALT',        'vK3QD7JwEZwyORFR1zyIclXJOZUrs1XN1juSYNVBKFFk6AYrVYqssRzbRbN6rIGH' );
define( 'SECURE_AUTH_SALT', 'k5zWOsyFA5zn6z8VPesurSrWQn70MI7GOCnRwwP4c8yABQWJkY3O3iE6vB348byy' );
define( 'LOGGED_IN_SALT',   '7JpSavGoqQQ1PQ7JJ5fwoFLryvKGc2U7NWROhmlhERoHgzaMQgneO2lhEa2sqdEa' );
define( 'NONCE_SALT',       'nFIWVVjt87p0SCOxRqUtHquEVOSuRzn9o3B2meXq9kzWCxoKn95eax50JEWuHNWP' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
